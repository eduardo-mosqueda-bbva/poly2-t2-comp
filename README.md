# poly2-t2-comp

Your component description.

Example:
```html
<poly2-t2-comp></poly2-t2-comp>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --poly2-t2-comp  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Mixin applied to :host font-family    | sans-serif  |
